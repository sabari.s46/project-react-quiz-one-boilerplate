import React, { Component } from 'react'
import '../App.css';
export default class Home extends Component {
  render() {
    return (
        
      <div className='Content'>
        <div className='Center'>
            <div className='Heading'>Quiz App</div>
            <div className='Button_cont'>Play</div>
        </div>
      </div>
    )
  }
}
