import React, { Component } from 'react'
import '../App.css';
export default class Quiz extends Component {
  render() {
    return (
        
      <div className='Content'>
        <div className="Quiz_Content">
            <div className="QHeading flex_center">Question</div>
            <div className="Question_Count">1 of 15</div>
            <div className='Question flex_center'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</div>
            <div className="Options ">
                <div className='Option1'>something</div>
                <div className='Option2'>something</div>
                <div className='Option3'>something</div>
                <div className='Option4'>something</div>
            </div>
            <div className="Controls ">
                <div className="Control1">Previous</div>
                <div className="Control2">Next</div>
                <div className="Control3">Quit</div>
            </div>
        </div>
      </div>
    )
  }
}
