import React, { Component } from 'react'

export default class Result extends Component {
  render() {
    return (
      <div className='Content'>
        <div className="Result_Content">
            <div className='Suggestion'>You need more practice</div>
            <div className="Score">Your score is 20%</div>
            <div className='Details'>
                <div className="details1 flex_spacebetween"><span>Total number of questions</span><span className="result1">45</span></div>
                <div className="details2 flex_spacebetween"><span>Total number of questions</span><span className="result2">45</span></div>
                <div className="details3 flex_spacebetween"><span>Total number of questions</span><span className="result3">45</span></div>
                <div className="details4 flex_spacebetween"><span>Total number of questions</span><span className="result4">45</span></div>
                
            </div>
            
        </div>
        <div className="buttons"><button>Play Again</button> <button>Back to Home</button></div>
      </div>
    )
  }
}
